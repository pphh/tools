package demo2;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author mh
 *
 * demo thread synchronization
 * 
 */

public class App {

    public static void main(String[] args) {
        
        App.logMessage("start...");
        
        BaseRunable run = new BaseRunable();
        Thread r1 = new Thread(run);
        Thread r2 = new Thread(run);
        r1.start();
        r2.start();
        
        //App.logMessage("sleep 3 seconds...");
        App.sleep(3);
        App.logMessage("slept 3 seconds.");
        
        BaseRunableSync runSync = new BaseRunableSync();
        Thread rs1 = new Thread(runSync);
        Thread rs2 = new Thread(runSync);
        rs1.start();
        rs2.start();
        
        App.logMessage("the end.");
        
    }
    
    public static void logMessage(String msg){
        String threadName = Thread.currentThread().getName();
        
        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);
        
        String info = String.format("[%s][%s] %s", strTime, threadName, msg);
        System.out.println(info);
    }
    
    public static void sleep(long seconds) {
        
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
    }

}
