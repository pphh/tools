package demo2;

import demo2.App;

public class BaseRunableSync implements Runnable {

    private int count = 10;
    
    public synchronized void run() {
        App.logMessage("start...");
        
        for( ; count>0; count--) {
            App.logMessage("count=" + count);
        }
        
        App.logMessage("the end.");
    }

}
