package demo6;

import java.text.SimpleDateFormat;
import java.util.Date;

import demo5.DeadlockRunable;

/**
 * @author mh
 *
 * demo an infinite loop in thread and print out thread status
 * 
 */

public class App {

    public static void main(String[] args) throws InterruptedException {
        
        App.logMessage("start...");
        
        InfiniteLoopRunable run = new InfiniteLoopRunable();
        Thread t1 = new Thread(run);
        App.logMessage("t1 status: " + t1.getState());
        
        t1.start();
        
        while(true) {
            App.logMessage("t1 status: " + t1.getState());
            
            Thread.sleep(1000);
            
            if( (t1.getState() == Thread.State.BLOCKED) 
             && (t1.getState() == Thread.State.TERMINATED)) {
                App.logMessage("t1 is blocked or terminated.");
                App.logMessage("t1 status: " + t1.getState());
                break;
            }
        }
        
        App.logMessage("the end.");
        
    }
    
    public static void logMessage(String msg){
        String threadName = Thread.currentThread().getName();
        
        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);
        
        String info = String.format("[%s][%s] %s", strTime, threadName, msg);
        System.out.println(info);
    }
    
    public static void sleep(long seconds) {
        
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
    }

}
