package demo3;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author mh
 *
 * demo how wait/sleep works with synchronized code block
 * 
 */

public class App {

    public static void main(String[] args) {
        
        App.logMessage("start...");
        
        Thread rs1 = null;
        Thread rs2 = null;
        
        // runnable + synchronized + no sleep/wait
        App.logMessage("runnable + synchronized + no sleep/wait");
        BaseRunableSync runSync = new BaseRunableSync();
        rs1 = new Thread(runSync);
        rs2 = new Thread(runSync);
        rs1.start();
        rs2.start();
        
        App.sleep(3);
        App.logMessage("slept 3 seconds.");
        
        // runnable + synchronized + sleep
        App.logMessage("runnable + synchronized + sleep");
        BaseRunableSync2 runSync2 = new BaseRunableSync2();
        rs1 = new Thread(runSync2);
        rs2 = new Thread(runSync2);
        rs1.start();
        rs2.start();
        
        App.sleep(15);
        App.logMessage("slept 15 seconds.");
        
        // runnable + synchronized + wait
        App.logMessage("runnable + synchronized + wait");
        BaseRunableSync3 runSync3 = new BaseRunableSync3();
        rs1 = new Thread(runSync3);
        rs2 = new Thread(runSync3);
        rs1.start();
        rs2.start();
        
        App.sleep(15);
        App.logMessage("slept 15 seconds.");
        
        App.logMessage("the end.");
        
    }
    
    public static void logMessage(String msg){
        String threadName = Thread.currentThread().getName();
        
        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);
        
        String info = String.format("[%s][%s] %s", strTime, threadName, msg);
        System.out.println(info);
    }
    
    public static void sleep(long seconds) {
        
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
    }

}
