package demo3;

import demo3.App;

public class BaseRunableSync2 implements Runnable {

    private int count = 10;
    
    public synchronized void run() {
        App.logMessage("start...");
        
        for( ; count>0; count--) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            App.logMessage("count=" + count);
        }
        
        App.logMessage("the end.");
    }

}
