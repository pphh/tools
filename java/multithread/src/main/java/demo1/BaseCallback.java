package demo1;

import java.util.concurrent.Callable;

public class BaseCallback implements Callable<Integer> {

    private int count;
    
    public BaseCallback(int count){
        this.count = count;
    }
    
    public Integer call() throws Exception {
        App.logMessage("start...");
        
        Integer total = 0;
        for( ; count>0; ) {
            //App.logMessage("count=" + count);
            total += count;
            count--;
        }
        
        App.logMessage("the end.");
        
        return total;
    }

}
