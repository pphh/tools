package demo1;

public class BaseThread extends Thread {

    private int count = 9;
    
    public BaseThread() {
        
    }
    
    public BaseThread(String name) {
        super(name);
    }
    
    public void run() {
        App.logMessage("start...");
        
        for( ; count>0; count--) {
            App.logMessage("count=" + count);
        }
        
        App.logMessage("the end.");
    }
}
