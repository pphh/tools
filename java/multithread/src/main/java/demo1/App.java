package demo1;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * @author mh
 *
 * demo serveral ways of thread implementation
 * 
 */

public class App {

    public static void main(String[] args) {
        
        App.logMessage("start...");
        
        // way 1
        BaseThread t1 = new BaseThread();
        BaseThread t2 = new BaseThread("Thread-new name");
        t1.start();
        t2.start();
        
        App.logMessage("sleep 3 seconds...");
        App.sleep(3);
        
        // way 2
        BaseRunnable run = new BaseRunnable();
        Thread r1 = new Thread(run, "Thread-run-1");
        Thread r2 = new Thread(run, "Thread-run-2");
        r1.start();
        r2.start();
        
        App.logMessage("sleep 3 seconds...");
        App.sleep(3);
        
        // way 3
        BaseRunnable cmd1 = new BaseRunnable();
        Executor executor = Executors.newFixedThreadPool(2);
        executor.execute(cmd1);
        
        BaseRunnable cmd2 = new BaseRunnable();
        ScheduledExecutorService scheduler = (ScheduledExecutorService) Executors.newScheduledThreadPool(10);;  
        scheduler.scheduleAtFixedRate(cmd2, 5, 1, TimeUnit.SECONDS); 
        
        App.logMessage("sleep 15 seconds...");
        App.sleep(15);
        scheduler.shutdown();
        
        // way 4
        ExecutorService exec1 = Executors.newCachedThreadPool(); 
        Future<Integer> result = exec1.submit(new BaseCallback(100));
        
        while(!result.isDone()){
            App.sleep(1);
        }
        
        try {
            App.logMessage("sum of 100: " + result.get());
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e1) {
            e1.printStackTrace();
        }
        
        exec1.shutdown();
        
        // way 5
        ExecutorService exec2 = Executors.newCachedThreadPool(); 
        ArrayList<Future<Integer>> results = new ArrayList<Future<Integer>>(); 
        for (int i = 0; i < 10; i++) {  
            results.add(exec2.submit(new BaseCallback(i)));  
        }
        
        App.logMessage("sleep 10 seconds...");
        App.sleep(15);
        
        for (Future<Integer> fs : results) {  
            if (fs.isDone()) {
                
                try {
                    App.logMessage(fs.get().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
            } else {  
                System.out.println("Future result is not complete yet.");
            }  
        }
        
        exec2.shutdown();
        
        App.logMessage("the end.");
        
    }
    
    public static void logMessage(String msg){
        String threadName = Thread.currentThread().getName();
        
        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);
        
        String info = String.format("[%s][%s] %s", strTime, threadName, msg);
        System.out.println(info);
    }
    
    public static void sleep(long seconds) {
        
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
    }

}
