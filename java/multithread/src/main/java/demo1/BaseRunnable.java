package demo1;

public class BaseRunnable implements Runnable {

    private int count = 9;
    
    public void run() {
        App.logMessage("start...");
        
        for( ; count>0; count--) {
            App.logMessage("count=" + count);
        }
        
        App.logMessage("the end.");
    }

}
