package demo5;

import java.text.SimpleDateFormat;
import java.util.Date;

import demo5.DeadlockRunable;

/**
 * @author mh
 *
 * demo a dead lock example in threads
 * 
 */

public class App {

    public static void main(String[] args) throws InterruptedException {
        
        App.logMessage("start...");
        
        
        DeadlockRunable run = new DeadlockRunable();
        Thread t1 = new Thread(run);
        Thread t2 = new Thread(run);
        App.logMessage("t1 status: " + t1.getState());
        App.logMessage("t2 status: " + t2.getState());
        
        t1.start();
        t2.start();
        
        
        while(true) {
            App.logMessage("t1 status: " + t1.getState());
            App.logMessage("t2 status: " + t2.getState());
            
            Thread.sleep(100);
            
            if( (t1.getState() == Thread.State.BLOCKED) 
             && (t2.getState() == Thread.State.BLOCKED) ) {
                App.logMessage("both t1 and t2 are blocked.");
                App.logMessage("t1 status: " + t1.getState());
                App.logMessage("t2 status: " + t2.getState());
                break;
            }
        }
        
        
//        App.logMessage("unlock t2 thread by interrupting t1 thread");
//        //t1.interrupt();
//        t1.stop();
//        while(true) {
//            App.logMessage("t1 status: " + t1.getState());
//            App.logMessage("t2 status: " + t2.getState());
//            
//            Thread.sleep(100);
//            
//            if( t2.getState() == Thread.State.TERMINATED ) {
//                App.logMessage("t2 is terminated.");
//                break;
//            }
//        }
        
        App.logMessage("the end.");
        
    }
    
    public static void logMessage(String msg){
        String threadName = Thread.currentThread().getName();
        
        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);
        
        String info = String.format("[%s][%s] %s", strTime, threadName, msg);
        System.out.println(info);
    }
    
    public static void sleep(long seconds) {
        
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
    }

}
