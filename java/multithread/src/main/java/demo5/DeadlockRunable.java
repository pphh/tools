package demo5;

import demo5.App;

public class DeadlockRunable implements Runnable {

    private Object lock1 = new Object();
    private Object lock2 = new Object();
    
    private Boolean bLockFlag = Boolean.FALSE;
    
//    public DeadlockRunable(Integer flag){
//        this.bLockFlag = flag;
//    }
    
    public void run() {
        App.logMessage("start...");
        bLockFlag = !bLockFlag;
        
        if(bLockFlag){
            
            synchronized(lock1){
                App.logMessage("lock1 is locked...");
                App.sleep(2);
                
                synchronized(lock2){
                    App.logMessage("lock2 is locked...");
                }
                
                App.logMessage("lock1 and lock2 is unlocked.");
            }
            
        }
        else {
            
            synchronized(lock2){
                App.logMessage("lock2 is locked...");
                App.sleep(2);
                
                synchronized(lock1){
                    App.logMessage("lock1 is locked...");
                }
                
                App.logMessage("lock1 and lock2 is unlocked.");
            }
            
        }
        
        
        App.logMessage("the end.");
    }

}
