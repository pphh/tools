package demo4;

import demo4.App;

public class BaseRunable implements Runnable {

    private int count = 10;
    
    public void run() {
        App.logMessage("start...");
        
        for( ; count>0; count--) {
            App.logMessage("count=" + count);
        }
        
        App.logMessage("the end.");
    }

}
