package demo4;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author mh
 *
 * demo thread join (wait for child threads to be finished)
 * 
 */

public class App {

    public static void main(String[] args) throws InterruptedException {
        
        App.logMessage("start...");
        
        BaseRunable run = new BaseRunable();
        Thread r1 = new Thread(run);
        Thread r2 = new Thread(run);
        r1.start();
        r2.start();
        
        // add the join method, which wait for r1/r2 to be finished
        r1.join();
        App.logMessage("received the end message of r1 thread.");
        r2.join();
        App.logMessage("received the end message of r2 thread.");
        
        App.logMessage("the end.");
        
    }
    
    public static void logMessage(String msg){
        String threadName = Thread.currentThread().getName();
        
        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);
        
        String info = String.format("[%s][%s] %s", strTime, threadName, msg);
        System.out.println(info);
    }
    
    public static void sleep(long seconds) {
        
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
    }

}
