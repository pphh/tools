package thumbnailator;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.Thumbnails.Builder;
import net.coobird.thumbnailator.geometry.Positions;
import net.coobird.thumbnailator.name.Rename;

/**
 * https://github.com/coobird/thumbnailator/wiki/Examples
 * @author mh
 * 
 */

public class App {

    public static void main(String[] args) {

        try {
            // Read image file - original and watermark
            URL url = System.class.getResource("/");
            String resDir = url.getPath();
            String originalImg = resDir + "bird.jpg";
            String watermarkImg = resDir + "watermark.jpg";
            
            File originFile = new File(originalImg);
            File watermarkFile = new File(watermarkImg);
            
            // Create a thumbnail from an image file
            Thumbnails.of(originFile)
            .size(150, 100)
            .toFile(new File(resDir + "thumbnail.jpg"));
            
            Builder<File> builder = Thumbnails.of(resDir + "bird.jpg");
            builder.size(150, 100)
            .toFile(resDir + "thumbnail2.jpg");
            
            // Scaling an image by a given factor
            BufferedImage img = ImageIO.read(originFile);
            BufferedImage thumbnail = Thumbnails.of(img)
                                                .scale(0.25)
                                                .asBufferedImage();
            ImageIO.write(thumbnail, "jpg", new File( resDir + "bird_scaled_25.jpg"));
            
            // Create a thumbnail with rotation
            Thumbnails.of(originFile)
            .scale(1)
            .rotate(90)
            .toFile(new File(resDir + "bird_with_rotation.jpg"));
            
            for (int i : new int[] {0, 90, 180, 270, 45}) {
                Thumbnails.of(originFile)
                .size(100, 100)
                .rotate(i)
                .toFile(new File(resDir + "bird_rotated_" + i + ".jpg"));
            }
            
            // Create a thumbnail with a watermark
            Thumbnails.of(originFile)
            .scale(1)
            .watermark(Positions.BOTTOM_RIGHT, ImageIO.read(watermarkFile), 0.3f)
            .outputQuality(0.8)
            .toFile(new File(resDir + "bird_with_watermark.jpg"));
            
            // Read jpg format file then write out as png format image
            OutputStream osPng = new FileOutputStream(resDir + "output.png");
            Thumbnails.of(originFile)
            .scale(1)
            .outputFormat("png")
            .toOutputStream(osPng);
            osPng.close();
            
            // process multiple images
            File outputDir = new File(resDir);
            Thumbnails.of(resDir+"bird.jpg", resDir+"watermark.jpg")
            .scale(1)
            .toFiles(outputDir, Rename.PREFIX_DOT_THUMBNAIL);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    

}
