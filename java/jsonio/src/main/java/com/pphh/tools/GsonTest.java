package com.pphh.tools;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class GsonTest {
    
    
    public void parseString() {
        String str = "{\"name\": \"jack\",\"age\": \"13\"}";
        
        Gson gson = new Gson();
        Person p = gson.fromJson(str, Person.class);
        
        App.printPersonInfo(p);
    }
    
    public void parseStringByFile(String eleFileName) {
        
        try {
            InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(eleFileName);
            String str = IOUtils.toString(input);
            
            Gson gson = new Gson();
            Person p = gson.fromJson(str, Person.class);
            
            App.printPersonInfo(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    public void parseElementByFile(String eleFilePath) {
        
        
        try {
            Gson gson = new Gson();
            FileReader fr = new FileReader(eleFilePath);
            JsonParser parser = new JsonParser();
            JsonElement data = parser.parse(fr);
            
            Gson gson1 = new Gson();
            Person p = gson1.fromJson(data, Person.class);
            
            App.printPersonInfo(p);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
    }
    
    public void parseArrayByFile(String arrFilePath) {
        
        

        try {
            Gson gson = new Gson();
            FileReader fr = new FileReader(arrFilePath);
            JsonParser parser = new JsonParser();
            JsonArray data = parser.parse(fr).getAsJsonArray();
            
            ArrayList<Person> persons = new ArrayList<Person>();
            for (JsonElement obj : data) {
                Person p = gson.fromJson(obj, Person.class);
                persons.add(p);
            }
            for (Person p : persons) {
                App.printPersonInfo(p);
            }
                   
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
    }
    
    
    public void test(String eleFilePath) {
        
        try {
            
            FileReader fr = new FileReader(eleFilePath);
            JsonReader reader = new JsonReader(fr);
            
            reader.beginObject();
            while (reader.hasNext()) {
                //String info = reader.nextName();
                String info;
                try {
                    info = reader.nextName();
                } catch (Exception e) {
                    info = reader.nextString();
                }
                App.logMessage(info);
                
            }
            reader.endObject();
            reader.close();
           
                   
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
