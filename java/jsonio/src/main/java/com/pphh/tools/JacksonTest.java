package com.pphh.tools;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonTest {
    
    public void parseArrayByFile(String arrFileName) {
        InputStream input = this.getClass().getClassLoader().getResourceAsStream(arrFileName);

        ObjectMapper mapper = new ObjectMapper();
        try {
            Person[] persons = mapper.readValue(input, Person[].class);
            
            for(Person p:persons){
                App.printPersonInfo(p);
            }
            
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
