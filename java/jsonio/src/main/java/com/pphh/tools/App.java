package com.pphh.tools;

//import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class App {

    public static void main(String[] args) {
        
        String eleFileName = "element.json";
        String arrFileName = "array.json";
        
        App.logMessage("--get resource directory--");
        String resDir = getResourcePath();
        
        App.logMessage("--gson test--");
        GsonTest gson = new GsonTest();
        gson.parseString();
        gson.parseStringByFile(eleFileName);
        gson.parseElementByFile(resDir + eleFileName);
        gson.parseArrayByFile(resDir + arrFileName);
        gson.test(resDir + eleFileName);
        
        App.logMessage("--jackson test--");
        JacksonTest jackson = new JacksonTest();
        jackson.parseArrayByFile(arrFileName);
        
    }
    
    public static void logMessage(String msg){

        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);
        
        String info = String.format("[%s] %s", strTime, msg);
        System.out.println(info);
    }
    
    public static void printPersonInfo(Person p){
        App.logMessage("person.name=" + p.getName());
        App.logMessage("person.age=" + p.getAge());
    }
    
    public static String getResourcePath() {

//        URL url = System.class.getResource("/");
//        if(url != null) {
//            String resDir = url.getPath();
//            App.logMessage("1 resDir = " + resDir);
//        }
//
//        url = Thread.currentThread().getContextClassLoader().getResource("");
//        if(url != null) {
//            String resDir = url.getPath();
//            App.logMessage("2 resDir = " + resDir);
//        }
//
//        url = ClassLoader.getSystemResource("");
//        if(url != null) {
//            String resDir = url.getPath();
//            App.logMessage("3 resDir = " + resDir);
//        }
        
        URL url = Thread.currentThread().getContextClassLoader().getResource("");
        return url.getPath();
    }

}
